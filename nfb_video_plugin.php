<?php
/**
 * @package NFB_Video_Plugin
 * @author National Film Board of Canada
 * @version 1.0.0
 */
/*
Plugin Name: NFB Video Plugin
Plugin URI: http://wordpress.org/extend/plugins/nfb-video-plugin/
Description: Plugin to embed NFB content into blogs
Author: National Film Board of Canada
Version: 1.0.0
Author URI: https://www.nfb.ca/
*/


include_once('nfb_functions.php');

function nfb_video_plugin_activate () {}

function nfb_video_plugin_deactivate () {}

function nfb_video_plugin_wp_head () {
    ?>
    <style>
    .embed-player-container {position: relative;padding-bottom: 56.25%; }
    .embed-player-container iframe,
    .embed-player-container object,
    .embed-player-container embed {position: absolute; border:0px; top: 0;left: 0;width: 100% !important;height: 100%  !important; z-index: 1;}
    </style>
<?php
}

register_activation_hook(__file__,'nfb_video_plugin_activate');
register_deactivation_hook(__file__,'nfb_video_plugin_deactivate');

add_filter('single_template', 'NFB_replace_links_with_embed');
add_action('wp_head', 'nfb_video_plugin_wp_head');

?>
