<?php

// Sift through the first 200 lines to find the code.

function NFB_url_table_exists()
{
    /*
    /	Checks to see that the database table for embeds is present.
    */
    global $wpdb;
    $table_name = $wpdb->prefix.'nfb_embeds';
    $table_exists = ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") == $table_name);
    if (!$table_exists):
          $sql = "
  			CREATE TABLE ". $table_name." (
  				cache_age int(6) NULL DEFAULT 0,
  			    html text NOT NULL,
  			    url varchar(300) NOT NULL,
  			    UNIQUE KEY (url)
  			);
  		";
      require_once(ABSPATH.'wp-admin/includes/upgrade.php');
      dbDelta($sql);
      return ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") == $table_name);
    else:
      return 1;
    endif;
}

function NFB_store_oembed($link, $html)
{
    global $wpdb;
    $table_name = $wpdb->prefix.'nfb_embeds';
    $sql = "INSERT INTO ".$table_name." (cache_age, html, url) VALUES(0, '" . $html . "', '" . $link . "');";
    $results = $wpdb->query($sql);
    return $results;
}

function NFB_retrieve_oembed($link)
{
    /*
    /	Gets the embed for a given link, or returns the link if it doesn't exist.
    */
    $trimmed_url = substr(trim($link), 2);

    if (NFB_url_table_exists()) {
        global $wpdb;
        $table_name = $wpdb->prefix.'nfb_embeds';
        $query = "SELECT * FROM " . $table_name . " WHERE url='" . $trimmed_url . "'";
        $results = $wpdb->get_results($query);
        if (count($results) > 0) {
            $html = $results[0]->html;
        } else {
            if (strpos($trimmed_url, ".nfb.ca")) {
                $oembed_url = "https://www.nfb.ca";
            } else {
                $oembed_url = "https://www.onf.ca";
            }
            $oembed_url = $oembed_url . "/remote/services/oembed/";
            $resource_url = $oembed_url . "?url=" . $trimmed_url . "&format=json";
            $data = json_decode(file_get_contents($resource_url));
            NFB_store_oembed($trimmed_url, $data->html);
            $html = $data->html;
        }

        $html = html_entity_decode($html);

        $html = '<div class="embed-player-container">'.$html;
        $html = str_replace("</iframe>", "</iframe></div>", $html);

        return $html;
    }
}

function NFB_get_links()
{
    global $post;
    if (substr($post->post_content, 0, 4) == 'http') {
        $post->post_content = ' '.$post->post_content;
    }
    preg_match_all('/\s?oehttps?:\/\/[0-9a-z_.\\w\/\\#~:?+=&;%@!-]*\?*[0-9a-z_.\\w\/\\#~:?+=&;%@!-]*/i', $post->post_content, $links);
    return $links[0];
}

function NFB_replace_links_with_embed($single_template)
{
    global $post;
    $links = NFB_get_links();
    foreach ($links as $oeLink) {
        $embed = NFB_retrieve_oembed($oeLink);
        $regex = '/\s?'.str_replace("?", "\?", str_replace("/", "\/", trim($link))).'/i';
        $post->post_content = str_replace($oeLink, $embed, $post->post_content);
    }
    return $single_template;
}
